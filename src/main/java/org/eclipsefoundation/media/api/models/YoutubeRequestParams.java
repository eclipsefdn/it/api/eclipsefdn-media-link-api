/*********************************************************************
* Copyright (c) 2022 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.media.api.models;

import jakarta.ws.rs.QueryParam;

/**
 * Entity used as a BeanParam for requests to the Youtube API. Contains all potential request parameters used by this application.
 */
public class YoutubeRequestParams {

    @QueryParam("key")
    public String key;

    @QueryParam("id")
    public String id;

    @QueryParam("channelId")
    public String channelId;

    @QueryParam("playlistId")
    public String playlistId;

    @QueryParam("pageToken")
    public String pageToken;

    @QueryParam("maxResults")
    public Integer maxResults;

    @QueryParam("part")
    public String part;
    
    public YoutubeRequestParams() {
        this.part = "id,snippet,player";
    }

}