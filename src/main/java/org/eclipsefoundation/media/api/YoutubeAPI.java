/*********************************************************************
* Copyright (c) 2022 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.media.api;

import org.eclipse.microprofile.rest.client.inject.RegisterRestClient;
import org.eclipsefoundation.media.api.models.YoutubeChannelResponse;
import org.eclipsefoundation.media.api.models.YoutubePlaylistItemResponse;
import org.eclipsefoundation.media.api.models.YoutubePlaylistResponse;
import org.eclipsefoundation.media.api.models.YoutubeRequestParams;
import org.eclipsefoundation.media.api.models.YoutubeVideoResponse;

import io.quarkus.vertx.http.Compressed;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.ws.rs.BeanParam;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;

/**
 * This rest client fetches video and playlist data from the Youtube API.
 * Individual playlists or videos can be looked up by id.
 */
@ApplicationScoped
@RegisterRestClient(configKey = "youtube-api")
public interface YoutubeAPI {

	/**
	 * Fetches a list of Youtube playlists. Query params part, id, and key
	 * are required.
	 * 
	 * @param params The required params for the endpoint
	 * @return A YoutubePlaylistResponse entity
	 */
	@GET
	@Compressed
	@Path("/playlists")
	YoutubePlaylistResponse getPlaylistsByChannel(@BeanParam YoutubeRequestParams params);

	/**
	 * Fetches data for a single Youtube video. Query params part, id, and key are
	 * required.
	 * 
	 * @param params The required params for the endpoint
	 * @return A YoutubeVideoResponse entity
	 */
	@GET
	@Compressed
	@Path("/videos")
	YoutubeVideoResponse getVideosById(@BeanParam YoutubeRequestParams params);

	/**
	 * Fetches all videos for a single Youtube playlist. Query params playlistId,
	 * part, and key are required.
	 * 
	 * @param params The required params for the endpoint
	 * @return A YoutubeChannelResponse Entity
	 */
	@GET
	@Compressed
	@Path("/playlistItems")
	YoutubePlaylistItemResponse getItemsByPlaylistId(@BeanParam YoutubeRequestParams params);

	/**
	 * Fetches content details for a single Youtube channel. Query params id, part,
	 * and key are required.
	 * 
	 * @param params The required params for the endpoint
	 * @return A YoutubeChannelResponse Entity
	 */
	@GET
	@Compressed
	@Path("/channels")
	YoutubeChannelResponse getChannelInfo(@BeanParam YoutubeRequestParams params);
}
