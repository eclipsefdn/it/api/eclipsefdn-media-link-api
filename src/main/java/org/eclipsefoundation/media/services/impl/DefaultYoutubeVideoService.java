/*********************************************************************
* Copyright (c) 2022 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.media.services.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import org.eclipse.microprofile.rest.client.inject.RestClient;
import org.eclipsefoundation.caching.model.ParameterizedCacheKeyBuilder;
import org.eclipsefoundation.caching.service.CachingService;
import org.eclipsefoundation.caching.service.LoadingCacheManager;
import org.eclipsefoundation.media.api.YoutubeAPI;
import org.eclipsefoundation.media.api.models.YoutubeRequestParams;
import org.eclipsefoundation.media.api.models.YoutubeVideoResponse;
import org.eclipsefoundation.media.config.YoutubeMediaProviderConfig;
import org.eclipsefoundation.media.models.YoutubeVideo;
import org.eclipsefoundation.media.services.YoutubeVideoService;
import org.eclipsefoundation.utils.helper.TransformationHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.quarkus.runtime.Startup;
import jakarta.annotation.PostConstruct;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.ws.rs.ProcessingException;
import jakarta.ws.rs.core.MultivaluedHashMap;

/**
 * Default implementation of YoutubeVideoService. Uses a loading cache video ids by channel. Also caches individual videos using the EF
 * CachingService.
 */
@Startup
@ApplicationScoped
public class DefaultYoutubeVideoService implements YoutubeVideoService {
    private static final Logger LOGGER = LoggerFactory.getLogger(DefaultYoutubeVideoService.class);

    private final YoutubeMediaProviderConfig config;
    private final LoadingCacheManager cacheManager;
    private final CachingService cache;
    private final YoutubeAPI api;

    /**
     * Default constructor for the Youtube video service, loading the cache layers, configuration, and auto-implemented YT API.
     * 
     * @param config mapping object for youtube provider config
     * @param cacheManager access to the loading cache for high availability
     * @param cache access to the standard cache layer
     * @param api YouTube API interface.
     */
    public DefaultYoutubeVideoService(YoutubeMediaProviderConfig config, LoadingCacheManager cacheManager, CachingService cache,
            @RestClient YoutubeAPI api) {
        this.api = api;
        this.cache = cache;
        this.cacheManager = cacheManager;
        this.config = config;
    }

    @PostConstruct
    public void init() {
        // Pre-load cache
        config
                .channels()
                .values()
                .parallelStream()
                .forEach(s -> cacheManager
                        .getList(ParameterizedCacheKeyBuilder
                                .builder()
                                .id(s)
                                .clazz(String.class)
                                .params(new MultivaluedHashMap<>())
                                .build()));
    }

    @Override
    public List<YoutubeVideo> getVideosById(String videoIds) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("Fetching Videos: {}", TransformationHelper.formatLog(videoIds));
        }

        // Split video ids into list
        List<String> ids = Arrays.asList(videoIds.split("\\s{0,4},\\s{0,4}"));
        if (ids.isEmpty()) {
            Collections.emptyList();
        }

        // Fetch each video and add to list if present
        List<YoutubeVideo> videos = new ArrayList<>();
        ids.forEach(id -> getVideoById(id).ifPresent(videos::add));
        return videos;
    }

    @Override
    public Optional<YoutubeVideo> getVideoById(String videoId) {
        // Validate the id before making request to YT API
        if (!isValidVideoId(videoId)) {
            if (LOGGER.isWarnEnabled()) {
                LOGGER.warn("Invalid video id: {}", TransformationHelper.formatLog(videoId));
            }
            return Optional.empty();
        }
        return fetchVideo(videoId);
    }

    /**
     * Fetches a video from the YT API and caches the result.
     * 
     * @param key the cache key/video id
     * @return A YoutubeVideo entity
     */
    private Optional<YoutubeVideo> fetchVideo(String videoId) {
        // save the sanitized version of the video ID for logging + exceptions
        String sanitizedVideoId = TransformationHelper.formatLog(videoId);
        LOGGER.debug("Fetching Video with ID: {}", sanitizedVideoId);

        // Fetch fingle video from YT API
        Optional<YoutubeVideoResponse> response = cache
                .get(videoId, new MultivaluedHashMap<>(), YoutubeVideoResponse.class,
                        () -> api.getVideosById(generateRequestParams(videoId)))
                .data();
        if (response.isEmpty()) {
            throw new ProcessingException(String.format("Error while loading video with id: %s", sanitizedVideoId));
        }

        return Optional.of(response.get().getItems().get(0));
    }

    /**
     * Iterates through the list of video ids and returns whether or not the requested videoId exists.
     * 
     * @param videoId the requestedc video id
     * @return true if match, false if not
     */
    private boolean isValidVideoId(String videoId) {
        List<String> videoIds;

        // For each channel id, check if id matches against cached list of video ids
        for (String channelId : config.channels().values()) {
            videoIds = cacheManager
                    .getList(ParameterizedCacheKeyBuilder
                            .builder()
                            .id(channelId)
                            .clazz(String.class)
                            .params(new MultivaluedHashMap<>())
                            .build());

            if (videoIds.stream().anyMatch(s -> s.equalsIgnoreCase(videoId))) {
                return true;
            }
        }
        return false;
    }

    /**
     * Generate base YT request params for requesting a video details.
     * 
     * @param videoId the internal ID of the video to request
     * @return the reuqest params to retrieve the specified video
     */
    private YoutubeRequestParams generateRequestParams(String videoId) {
        YoutubeRequestParams params = new YoutubeRequestParams();
        params.id = videoId;
        params.key = config.apiKey();
        return params;
    }
}
