/*********************************************************************
* Copyright (c) 2023 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.media.precaches;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.eclipse.microprofile.rest.client.inject.RestClient;
import org.eclipsefoundation.caching.model.ParameterizedCacheKey;
import org.eclipsefoundation.caching.service.CachingService;
import org.eclipsefoundation.caching.service.LoadingCacheManager.LoadingCacheProvider;
import org.eclipsefoundation.media.api.YoutubeAPI;
import org.eclipsefoundation.media.api.models.YoutubeChannelResponse;
import org.eclipsefoundation.media.api.models.YoutubePlaylistItemResponse;
import org.eclipsefoundation.media.api.models.YoutubeRequestParams;
import org.eclipsefoundation.media.config.YoutubeMediaProviderConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Named;
import jakarta.ws.rs.ProcessingException;
import jakarta.ws.rs.core.MultivaluedHashMap;

/**
 * Precache provider for Youtube video ids.
 */
@Named("videos")
@ApplicationScoped
public class YoutubeVideoPrecacheProvider implements LoadingCacheProvider<String> {
    private static final Logger LOGGER = LoggerFactory.getLogger(YoutubeVideoPrecacheProvider.class);

    private static final String PART = "contentDetails";

    // the max number of results that can be pulled from a YT video endpoint in a single call
    public static final Integer MAX_RESULTS_VIDEO_RESPONSE = 50;

    private final YoutubeMediaProviderConfig config;
    private final YoutubeAPI api;
    private final CachingService cache;

    /**
     * Default constructor for precaching youtube video data.
     * 
     * @param config mapping object for youtube provider config
     * @param cache access to the standard cache to store results
     * @param api interface for calling the Youtube API
     */
    public YoutubeVideoPrecacheProvider(YoutubeMediaProviderConfig config, CachingService cache, @RestClient YoutubeAPI api) {
        this.api = api;
        this.config = config;
        this.cache = cache;
    }

    @Override
    public List<String> fetchData(ParameterizedCacheKey k) {
        try {
            LOGGER.info("Caching video ids for channel: {}", k.id());

            // Get playlist id for channel's "all" playlist
            String playlistId = getChannelUploadsId(k.id());

            // Get first page of results, using max amount
            YoutubePlaylistItemResponse response = api.getItemsByPlaylistId(generatePlaylistRequestParams(playlistId, Optional.empty()));
            // if something changes in the channel, we shouldn't crash the call
            if (response == null) {
                LOGGER.warn("No base playlist can be found for channel '{}', returning empty list of videos", k.id());
                return Collections.emptyList();
            }

            // Add each video id to our final list
            List<String> out = response.getItems().stream().map(i -> i.getContentDetails().getVideoId()).collect(Collectors.toList());

            // Iterate over every page of YT API results
            while (response.getNextPageToken() != null) {
                response = api.getItemsByPlaylistId(generatePlaylistRequestParams(playlistId, Optional.of(response.getNextPageToken())));

                // Add each additional video id to our final list
                out.addAll(response.getItems().stream().map(i -> i.getContentDetails().getVideoId()).toList());
            }

            return out;
        } catch (Exception e) {
            throw new ProcessingException(String.format("Error caching video ids for channel: %s", k.id()), e);
        }
    }

    @Override
    public Class<String> getType() {
        return String.class;
    }

    /**
     * Fetches the uploads playlist id for the given channel.
     * 
     * @param channelId The desired channel's id
     * @return A string containing the playlist id, or null.
     */
    private String getChannelUploadsId(String channelId) {
        LOGGER.debug("Fetching UploadID for channel: {}", channelId);
        Optional<YoutubeChannelResponse> response = cache
                .get(channelId, new MultivaluedHashMap<>(), YoutubeChannelResponse.class,
                        () -> api.getChannelInfo(generateChannelRequestParams(channelId)))
                .data();
        if (response.isEmpty()) {
            LOGGER.warn("Error fetching UploadID for channel: {}", channelId);
            return null;
        }

        return response.get().getItems().get(0).getContentDetails().getRelatedPlaylists().getUploads();
    }

    /**
     * Generate base YT request params for requesting a channel details.
     * 
     * @param channelId the internal ID of the channel to request
     * @return the reuqest params to retrieve the specified channel details
     */
    private YoutubeRequestParams generateChannelRequestParams(String channelId) {
        YoutubeRequestParams params = new YoutubeRequestParams();
        params.id = channelId;
        params.part = PART;
        params.key = config.apiKey();
        return params;
    }

    /**
     * Generate base YT request params for requesting a playlist details.
     * 
     * @param playlistId the internal ID of the playlist to request
     * @return the reuqest params to retrieve the specified playlist details
     */
    private YoutubeRequestParams generatePlaylistRequestParams(String playlistId, Optional<String> next) {
        YoutubeRequestParams params = new YoutubeRequestParams();
        params.playlistId = playlistId;
        params.part = PART;
        params.maxResults = MAX_RESULTS_VIDEO_RESPONSE;
        params.key = config.apiKey();
        next.ifPresent(n -> params.pageToken = n);
        return params;
    }
}
