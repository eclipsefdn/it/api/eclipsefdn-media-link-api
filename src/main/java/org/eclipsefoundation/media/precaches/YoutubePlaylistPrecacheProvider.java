/*********************************************************************
* Copyright (c) 2023 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.media.precaches;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.eclipse.microprofile.rest.client.inject.RestClient;
import org.eclipsefoundation.caching.model.ParameterizedCacheKey;
import org.eclipsefoundation.caching.service.LoadingCacheManager.LoadingCacheProvider;
import org.eclipsefoundation.media.api.YoutubeAPI;
import org.eclipsefoundation.media.api.models.YoutubePlaylistResponse;
import org.eclipsefoundation.media.api.models.YoutubeRequestParams;
import org.eclipsefoundation.media.config.YoutubeMediaProviderConfig;
import org.eclipsefoundation.media.models.YoutubePlaylist;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Named;
import jakarta.ws.rs.ProcessingException;

/**
 * Precache provider for YoutubePlaylist entities.
 */
@Named("playlists")
@ApplicationScoped
public class YoutubePlaylistPrecacheProvider implements LoadingCacheProvider<YoutubePlaylist> {
    private static final Logger LOGGER = LoggerFactory.getLogger(YoutubePlaylistPrecacheProvider.class);

    // the max number of results that can be pulled from a YT Playlist endpoint in a single call
    public static final Integer MAX_RESULTS_PLAYLIST_RESPONSE = 50;

    private final YoutubeMediaProviderConfig config;
    private final YoutubeAPI api;

    /**
     * Default constructor for the Youtube playlist precache, loading configuration and YT API interface.
     * 
     * @param config mapping object for youtube provider config
     * @param api YouTube API interface.
     */
    public YoutubePlaylistPrecacheProvider(YoutubeMediaProviderConfig config, @RestClient YoutubeAPI api) {
        this.api = api;
        this.config = config;
    }

    @Override
    public List<YoutubePlaylist> fetchData(ParameterizedCacheKey k) {
        try {
            LOGGER.info("Caching playlists for channel: {}", k.id());

            // Get first page of results, using max amount
            YoutubePlaylistResponse response = api.getPlaylistsByChannel(generateChannelRequestParams(k.id(), Optional.empty()));

            List<YoutubePlaylist> out = new ArrayList<>(response.getItems());

            // Iterate over every page of YT API results
            while (response.getNextPageToken() != null) {
                response = api.getPlaylistsByChannel(generateChannelRequestParams(k.id(), Optional.of(response.getNextPageToken())));

                // Add each item to our final list
                out.addAll(response.getItems());
            }

            // Update all embed URLs to https before adding to cache
            return out.stream().map(this::updateEmbedUrlToHttps).toList();
        } catch (Exception e) {
            throw new ProcessingException(String.format("Error while loading cache entry for: %s", k.id()), e);
        }
    }

    @Override
    public Class<YoutubePlaylist> getType() {
        return YoutubePlaylist.class;
    }

    /**
     * Updates a YoutubePlaylist entity's embed URL from http to https.
     * 
     * @param src The YoutubePlaylist to update
     * @return The updated YoutubePlaylist with https embed URL
     */
    private YoutubePlaylist updateEmbedUrlToHttps(YoutubePlaylist src) {
        String currentEmbed = src.getPlayer().getEmbedHtml();
        String secureEmbed = currentEmbed.replaceFirst("http:", "https:");

        // Update embed URL with https
        return src.toBuilder().setPlayer(src.getPlayer().toBuilder().setEmbedHtml(secureEmbed).build()).build();
    }

    /**
     * Generate base YT request params for requesting a playlist details.
     * 
     * @param playlistId the internal ID of the playlist to request
     * @return the reuqest params to retrieve the specified playlist details
     */
    private YoutubeRequestParams generateChannelRequestParams(String channelId, Optional<String> next) {
        YoutubeRequestParams params = new YoutubeRequestParams();
        params.channelId = channelId;
        params.maxResults = MAX_RESULTS_PLAYLIST_RESPONSE;
        params.key = config.apiKey();
        next.ifPresent(n -> params.pageToken = n);
        return params;
    }
}
