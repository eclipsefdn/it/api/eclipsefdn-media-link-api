/*********************************************************************
* Copyright (c) 2022 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.media.test.helpers;

public class SchemaNamespaceHelper {
    public static final String BASE_SCHEMA_PATH = "schemas/";
    public static final String BASE_SCHEMA_PATH_SUFFIX = "-schema.json";

    public static final String VIDEO_SCHEMA_PATH = BASE_SCHEMA_PATH + "video" + BASE_SCHEMA_PATH_SUFFIX;

    public static final String VIDEOS_SCHEMA_PATH = BASE_SCHEMA_PATH + "videos" + BASE_SCHEMA_PATH_SUFFIX;

    public static final String PLAYLIST_SCHEMA_PATH = BASE_SCHEMA_PATH + "playlist" + BASE_SCHEMA_PATH_SUFFIX;

    public static final String PLAYLISTS_SCHEMA_PATH = BASE_SCHEMA_PATH + "playlists" + BASE_SCHEMA_PATH_SUFFIX;

    public static final String ERROR_SCHEMA_PATH = BASE_SCHEMA_PATH + "error" + BASE_SCHEMA_PATH_SUFFIX;
}
